﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Assignment01
{              
    public class Program
    {
        public static void Main(string[] args)
        {
            ShoppingController test = new ShoppingController();
            test.run();
        }
    }
}
