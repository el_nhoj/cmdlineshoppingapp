﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment01
{
    public class SubSystem
    {
        private Inventory inventory = new Inventory();
        private Cart cart = new Cart();

        public SubSystem() { }

        // General purpose method to convert user input strings into integer
        public int getInputAsInt(String prompt)
        {
            Console.WriteLine(prompt);
            while (true)
            {
                try
                {
                    int value = Convert.ToInt32(Console.ReadLine());

                    if (value <= 0)
                        Console.WriteLine("Input cannot be a zero or negative value. Please try again.");
                    else
                        return value;
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid input. Please try again.");
                    continue;
                }
            }
        }

        // General purpose method to convert user input strings into char
        public char getInputAsChar(String prompt)
        {
            Console.WriteLine(prompt);
            while (true)
            {
                try
                {
                    char value = Convert.ToChar(Console.ReadLine());

                    if (Char.IsLetter(value))
                        return value;
                    else
                        Console.WriteLine("Input was not an alphabetical letter. Please try again.");
                }
                catch (FormatException)
                {
                    Console.WriteLine("Invalid input. Please try again.");
                    continue;
                }
            }
        }

        // Method to prompt user to add products to cart
        public void addPrompt()
        {
            Console.WriteLine();
            inventory.viewInventory();

            // Get user inputs for product code and quantity
            int prodCode = getInputAsInt("Please enter a product code: ");
            int qtyAmount = getInputAsInt("Please enter the quantity of product you want to purchase: ");

            if (inventory.checkInventory(prodCode, qtyAmount) == true)
            {
                // Get matched product reference and create a temporary object of it
                Product temp = inventory.findProductByCode(prodCode);

                // Create new product to add to cart and discard temp object
                cart.addToCart(new Product(temp.recordNumber, temp.name, temp.stock, temp.price), qtyAmount);
                Console.WriteLine("{0}\n{1}", (temp.name + " (x" + qtyAmount + ") added successfully!"),
                    "Press Enter to return to main menu . . .");
                Console.ReadLine();
                inventory.updateInventory(prodCode, -qtyAmount);
                temp = null;
            }
            else
            {
                Console.WriteLine("{0}\n{1}", "Product code does not exist or quantity entered exceeds stock.",
                    "Press Enter to return to previous menu . . .");
                Console.ReadLine();
            }
        }

        // Method to prompt user to remove products from cart
        public void removePrompt()
        {
            if (cart.isCartEmpty() == true)
            {
                Console.WriteLine("{0}\n{1}", "Cannot remove a product if cart is empty!",
                   "Press Enter to return to previous menu . . .");
                Console.ReadLine();
            }
            else
            {
                cart.viewCart();
                int prodCode = getInputAsInt("Please enter a product code: ");

                if (cart.isProductInCart(prodCode) == true)
                {
                    int qtyAmount = cart.returnProductQty(prodCode);
                    cart.removeFromCart(prodCode);
                    inventory.updateInventory(prodCode, qtyAmount);
                }
                else
                {
                    Console.WriteLine("{0}\n{1}", "Product code not found or does not exist.",
                        "Press Enter to return to previous menu . . .");
                    Console.ReadLine();
                }
            }
        }

        // Method to display the cart prompt
        public void cartPrompt()
        {
            cart.viewCart();
            Console.WriteLine("Press Enter to return to previous menu . . .");
            Console.ReadLine();
        }

        // Method to prompt user to checkout and pay for their cart
        public void checkoutPrompt()
        {
            // If cart is empty display error message, otherwise display order information
            if (cart.isCartEmpty() == true)
            {
                Console.WriteLine("{0}\n{1}", "Cannot checkout when cart is empty!",
                   "Press Enter to return to previous menu . . .");
                Console.ReadLine();
            }
            else
            {
                cart.viewCart();
                char choice = getInputAsChar("Confirm checkout and place your order (Y/N)?");

                // If input is 'y', write current inventory to file and clear cart 
                if (choice == 'Y' || choice == 'y')
                {
                    inventory.writeToXML();
                    cart.clearCart();
                }
            }
        }
    }
}
