﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Linq;
using System.Threading.Tasks;

namespace Assignment01
{
    public class Inventory
    {
        private List<Product> listOfProducts = new List<Product>();

        // Creating an instance of Inventory will read products from product.inventory.xml
        // to create and add Product objects to the list from the XML file's values  
        public Inventory()
        {
            // Throw exception if file can't be loaded and terminate
            try
            {
                XDocument xdoc = XDocument.Load("product.inventory.xml");

                var products = from product in xdoc.Descendants("product")
                               select product;

                foreach (var field in products)
                {
                    int r = Convert.ToInt32(field.Element("recordNumber").Value);
                    string n = Convert.ToString(field.Element("name").Value);
                    int s = Convert.ToInt32(field.Element("stock").Value);
                    double p = Convert.ToDouble(field.Element("price").Value);

                    listOfProducts.Add(new Product(r, n, s, p));
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("{0}\n{1}", "XML file 'product.inventory.xml' cannot be found or loaded.",
                    "Terminating program . . .");
                Console.ReadLine();
                Console.Beep();
                Environment.Exit(1);
            }
        }

        // Method to check if product in inventory exists and has stock
        public bool checkInventory(int prodCode, int qtyAmount)
        {
            int match = listOfProducts.FindIndex(p =>
                (p.recordNumber == prodCode) && (p.stock >= qtyAmount)
            );

            return (match >= 0) ? true : false;
        }

        // Method to find if a product exists by its product code and returns it
        public Product findProductByCode(int prodCode)
        {
            try
            {
                Product match = listOfProducts.First(p => p.recordNumber == prodCode);
                return match;
            }
            catch (ShoppingException)
            {
                Console.WriteLine("Product not found or does not exist.");
                return null;
            }            
        }

        // Method to update inventory quantity
        public void updateInventory(int prodCode, int qtyAmount)
        {
            Product match = findProductByCode(prodCode);

            if (match != null)
                match.setProductQty((match.stock + qtyAmount));           
        }

        // Method to write inventory to XML file
        public void writeToXML() 
        {
            try
            {
                var xInventory = new XElement("inventory",

                from p in listOfProducts

                select new XElement("product",
                    new XElement("recordNumber", p.recordNumber),
                    new XElement("name", p.name),
                    new XElement("stock", p.stock),
                    new XElement("price", p.price)
                ));

                xInventory.Save("product.inventory.xml");
            }
            catch(ShoppingException)
            {
                Console.WriteLine("Failed to write to file. Please press Enter to continue . . .");
                Console.ReadLine();
            }
        }

        // Method to display inventory
        public void viewInventory()
        {
            Console.WriteLine(" -----------------------------------------------");
            Console.WriteLine("| {0,-5} | {1,-13} | {2,-7} | {3,11} |", "Code",
                    "Name", "Stock", "Price");
            Console.WriteLine(" -----------------------------------------------");
            foreach (Product p in listOfProducts)
            {
                Console.WriteLine("| {0,-5} | {1,-13} | {2,-7} | {3,11:0.00} |",
                    p.recordNumber, p.name, p.stock, p.price);
            }
            Console.WriteLine(" -----------------------------------------------\n");
        }
    }
}
