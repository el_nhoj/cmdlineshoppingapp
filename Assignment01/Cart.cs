﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment01
{
    public class Cart
    {
        private List<Product> listOfCartItems = new List<Product>(); 

        public double totalPrice { get; private set; }

        public Cart()
        {
            totalPrice = 0.00;
        }

        // Method to find if a product exists by its product code
        public Product findProductByCode(int prodCode) 
        {
            try
            {
                Product match = listOfCartItems.Find(p => p.recordNumber == prodCode);
                return match;
            }
            catch (ShoppingException)
            {
                Console.WriteLine("Product not found or does not exist.");
                return null;
            }
        }

        // Method to calculate and display cart total
        public void calculateTotal()
        {
            double subTotal = 0.00;

            foreach(Product p in listOfCartItems)
            {
                subTotal += (p.stock * p.price);
            }

            totalPrice = subTotal;
        }

        // Method to check if cart is empty
        public bool isCartEmpty()
        {
            return (!listOfCartItems.Any()) ? true : false;
        }

        // Method to check if a product exists in the cart
        public bool isProductInCart(int prodCode)
        {
            Product match = findProductByCode(prodCode);

            return (match != null) ? true : false;
        }

        // Method to add product(s) to cart
        public void addToCart(Product p, int qtyAmount)
        {
            //Check if product exists in cart
            Product match = findProductByCode(p.recordNumber);

            // If match is true, update quantity and price
            if (match != null)
            {
                match.setProductQty((qtyAmount + match.stock));
            }
            else
            {
                // If false, add product to cart
                p.setProductQty(qtyAmount);
                listOfCartItems.Add(p);
            }
        }

        // Method to remove product(s) from cart
        public void removeFromCart(int prodCode)
        {
            Product match = findProductByCode(prodCode);
            
            listOfCartItems.Remove(match);
            Console.WriteLine("{0}\n{1}", "Product removed successfully!",
               "Press Enter to return to previous menu . . .");
            Console.ReadLine();
        }

        // Method to clear the cart
        public void clearCart() 
        {
            listOfCartItems.Clear();
            Console.WriteLine("{0}\n{1}", "Your order has been placed successfully!",
                        "Press Enter to return to previous menu . . .");
            Console.ReadLine();
        }

        // Method to view cart contents
        public void viewCart()
        {
            Console.WriteLine("\n -----------------------------------------------");
            Console.WriteLine("| {0} {1,26} |", "Your Shopping Cart", "");
            Console.WriteLine(" -----------------------------------------------");
            Console.WriteLine("| {0,-5} | {1,-13} | {2,-7} | {3,11} |", "Code",
                    "Name", "Qty", "Subtotal");
            Console.WriteLine(" -----------------------------------------------");

            // Checks if cart is empty and displays message or contents accordingly
            if (isCartEmpty() == true)
            {
                Console.WriteLine("| {0,-5} {1} {2,6} |", " ", "There are no items in your cart.", " ");
            }
            else
            {
                foreach (Product p in listOfCartItems)
                {
                    Console.WriteLine("| {0,-5} | {1,-13} | {2,-7} | {3,11:0.00} |",
                        p.recordNumber, p.name, p.stock, (p.stock * p.price));
                }
            }

            calculateTotal();
            Console.WriteLine(" -----------------------------------------------");
            Console.WriteLine("| {0} {1,-35:0.00} {2}|", "Total: $", totalPrice, " ");
            Console.WriteLine(" -----------------------------------------------\n");            
        }
                
        // Method to return a carted product's quantity
        public int returnProductQty(int prodCode)
        {
            Product match = findProductByCode(prodCode);
            return match.stock; 
        }
    }
}
