﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment01
{
    // I implemented the Facade design pattern to simplify this class (originally the primary controller)
    // and reduce its responsibilities, which is instead, now delegated to the SubSystem class. This 
    // generally makes things look neater, easier to understand and hides my messy thought 
    // processes/implementations.

    // Facade Class
    public class ShoppingController
    {
        private SubSystem sub = new SubSystem();
        private Menu mainMenu = new Menu("WELCOME TO CONSOLE BASED SHOPPING CART!", 
            new String[]{"Add an item to cart", "Remove an item from the cart", "View the cart", 
                "Checkout and Pay", "Exit"
            });

        public ShoppingController() {}

        // Method that executes and runs the shopping system
        public void run()
        {
            Boolean running = true;
            
            do
            {
                int option = mainMenu.viewMenu();
                switch (option)
                {
                    case 1: sub.addPrompt();
                        break;
                    case 2: sub.removePrompt();
                        break;
                    case 3: sub.cartPrompt();
                        break;
                    case 4: sub.checkoutPrompt();
                        break;
                    case 5:
                        Console.Write("Exiting program . . .");
                        Console.Beep();
                        running = false;
                        break;
                    default:
                        Console.WriteLine("Invalid option. Please try again.");
                        break;
                }
            }
            while (running);
        }
    }
}
