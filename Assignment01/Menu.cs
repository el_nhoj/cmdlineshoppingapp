﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment01
{
    public class Menu
    {
        public String[] menuItems { get; private set; }
        public String title { get; private set; }

        public Menu(String title, String[] menuItems)
        {
            this.title = title;
            this.menuItems = menuItems;
        }

        // Method for calling and displaying an instantiated menu that takes user input  
        public int viewMenu()
        {
            while (true)
            {
                int i = 0;
                Console.WriteLine(title + "\n");
                Console.WriteLine("Following options are available: \n\n");

                foreach (String item in menuItems)
                {
                    Console.WriteLine("{0}. {1}\n", ++i, item);
                }
                Console.WriteLine();

                while (true)
                {
                    Console.WriteLine("Please select an option [{0}-{1}]:", 1, menuItems.Length);
                    string input = Console.ReadLine();
                    try
                    {
                        int value = Convert.ToInt32(input);
                        if (value > 0 && value <= menuItems.Length)
                            return value;
                        else
                            Console.Write("Invalid menu choice. ");
                    }
                    catch (FormatException)
                    {
                        Console.Write("Invalid input. ");
                        continue;
                    }
                }
            }
        }
    }
}
