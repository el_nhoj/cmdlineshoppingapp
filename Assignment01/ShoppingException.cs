﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment01
{
    // Custom-defined exception class
    public class ShoppingException : Exception
    {
        public ShoppingException() { }
        public ShoppingException(string message)
            : base(message)
        {
        }

        public ShoppingException(string message, Exception inner)
            : base(message, inner)
        {
        }

    }
}
