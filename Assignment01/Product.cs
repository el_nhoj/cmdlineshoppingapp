﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment01
{
    public class Product
    {
        public int recordNumber { get; private set; }
        public string name { get; private set; }
        public int stock { get; private set; }
        public double price { get; private set; }

        public Product(int recordNumber, string name, int stock, double price)
        {
            this.recordNumber = recordNumber;
            this.name = name;
            this.stock = stock;
            this.price = price;
        }

        // Mutator method to update a product's stock/quantity
        public void setProductQty(int qty) 
        {
            stock = qty;
        }

    }
}
